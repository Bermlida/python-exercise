def add2number(a, b):
    global d
    c = a + b
    d = a + b

    print("in add2number: c = {}, d = {}" . format(c, d))
    return c

c = 10
d = 99

print("before add2number: c = {}, d = {}" . format(c, d))
print("{} + {} = {}" . format(2, 2, add2number(2, 2)))
print("after add2number: c = {}, d = {}" . format(c, d))