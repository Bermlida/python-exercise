import re

fp = open("sample.txt", "r")
article = fp.read()
new_article = re.sub("[^a-zA-Z\s]", "", article)
words = new_article.split()
word_counts = {}

for word in words:
    if word.upper() in word_counts:
        word_counts[word.upper()] = word_counts[word.upper()] + 1
    else:
        word_counts[word.upper()] = 1

# 以 單字(key) 為排序依據
#key_list = list(word_counts.keys())
#key_list.sort()
#key_list = list(word_counts.keys())
#for key in key_list:
#    if word_counts[key] > 1:
#        print("{}:{}" . format(key, word_counts[key]))

#以單字出現的 頻率高低(低到高) 為排序依據
word_counts = sorted(word_counts.items(), key = lambda d:d[1])
for word in word_counts:
    if word[1] > 1:
        print("{}:{}".format(word[0], word[1]))