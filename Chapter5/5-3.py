import random

game_count = 0
all_counts = []

while True:
    game_count += 1
    guess_count = 0
    answer = random.randint(0, 99)

    while True:
        guess = int(input("請猜一個數字 (0-99)："))
        guess_count += 1

        if guess == answer:
            print("猜中了，共猜了 " + str(guess_count) + " 次")
            all_counts.append(guess_count)
            break
        else:
            if abs(guess - answer) <= 3:
                print("只差一點點")
            else:
                print("太大了" if guess > answer else "太小了")

    onemore = input("再玩一次(Y/N) ? ")
    if onemore != 'Y' and onemore != 'y':
        print("成績如下：")
        print(all_counts)
        print("平均猜中次數 " + \
              str(sum(all_counts) / float(len(all_counts))))
        break