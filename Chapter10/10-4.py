
import datetime
import hashlib
import json
import os.path
import requests

url = 'http://earthquake.usgs.gov/earthquakes/feed/v1.0/summary/4.5_week.geojson'
resource = requests.get(url)
sig = hashlib.md5(resource.text.encode('utf-8')).hexdigest()
old_sig = ''

if os.path.exists('resources/eq_sig.txt'):
    with open('resources/eq_sig.txt', 'r') as fp:
        old_sig = fp.read()
    with open('resources/eq_sig.txt', 'w') as fp:
        fp.write(sig)
else:
    with open('resources/eq_sig.txt', 'w') as fp:
        fp.write(sig)

if sig == old_sig:
    print('data is current')

choice = input('show data(y/Y: show data ; other: not): ')
if choice == 'Y' or choice == 'y':
    earthquakes = json.loads(resource.text)
    for earthquake in earthquakes['features']:
        earthquake_time = float(earthquake['properties']['time']) / 1000.0
        earthquake_time = datetime.datetime.fromtimestamp(earthquake_time) . strftime('%Y-%m-%d %H:%M:%S')
        print('time:{} mag:{} place:{}' . format(
            earthquake_time,
            earthquake['properties']['mag'],
            earthquake['properties']['place']
        ))


