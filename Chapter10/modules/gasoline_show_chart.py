
import matplotlib.pyplot
import numpy
import sqlite3

def chart():
    data = []
    conn = sqlite3.connect('resources/gasoline.sqlite')
    cursor = conn.execute('select * from prices order by gdate;')

    for row in cursor:
        data.append(list(row))

    x = numpy.arange(0, len(data))
    dataset = [list(), list(), list()]
    for i in range(0, len(data)):
        for j in range(0, 3):
            dataset[j].append(data[i][j + 1])

    w = numpy.array(dataset[0])
    y = numpy.array(dataset[1])
    z = numpy.array(dataset[2])

    matplotlib.pyplot.ylabel("NTD$")
    matplotlib.pyplot.xlabel("Weeks({} --- {})" . format(data[0][0], data[len(data) - 1][0]))

    matplotlib.pyplot.plot(x, w, color="blue", label="92")
    matplotlib.pyplot.plot(x, y, color="red", label="95")
    matplotlib.pyplot.plot(x, y, color="green", label="98")

    matplotlib.pyplot.xlim(0, len(data))
    matplotlib.pyplot.ylim(10, 40)
    matplotlib.pyplot.title("Gasoline Prices")
    matplotlib.pyplot.legend()
    matplotlib.pyplot.show()