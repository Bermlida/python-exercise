
from selenium import webdriver

def show_webpage(url, screenshot_name = ""):
    web = webdriver.Chrome()

    web.set_window_position(0, 0)
    web.set_window_size(800, 600)

    web.get(url)

    if screenshot_name != "":
        web.save_screenshot(screenshot_name)



def get_webpage_handler(url):
    web = webdriver.Chrome()

    web.get(url)

    return web
