
from bs4 import BeautifulSoup
import requests
import sqlite3

def fetch_data():
    conn = sqlite3.connect('resources/gasoline.sqlite')
    url = 'http://new.cpc.com.tw/division/mb/oil-more4.aspx'

    html = requests.get(url).text
    sp = BeautifulSoup(html, 'html.parser')
    data = sp.find_all('span', {'id': 'Showtd'})
    rows = data[0].find_all('tr')

    prices = list()
    for row in rows:
        cols = row.find_all('td')
        if len(cols[1].text) > 0:
            item = [cols[0].text, cols[1].text, cols[2].text, cols[3].text]
        prices.append(item)

    for price in prices:
        sqlstr = "select * from prices where gdate='{}';" . format(price[0])
        cursor = conn.execute(sqlstr)
        if len(cursor.fetchall()) == 0:
            g92 = 0 if price[1] == '' else float(price[1])
            g95 = 0 if price[2] == '' else float(price[2])
            g98 = 0 if price[3] == '' else float(price[3])

            sqlstr = "insert into prices values('{}', {}, {}, {});" . format(price[0], g92, g95, g98)
            print(sqlstr)
            conn.execute(sqlstr)