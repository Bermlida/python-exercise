from Chapter10.modules import gasoline_fetch_data
from Chapter10.modules import gasoline_show_data
from Chapter10.modules import gasoline_show_chart

while True:
    print("Gasoline Prices Query System")
    print("--------------------------------------")
    print("1. sync lasted oli price")
    print("2. show past oli price")
    print("3. show past 10 week oli price")
    print("4. show oli chart")
    print("0. exit")
    print("--------------------------------------")

    choice = int(input("press number to input choice: "))
    if choice == 1:
        gasoline_fetch_data.fetch_data()
    elif choice == 2:
        gasoline_show_data.disp_alldata()
    elif choice == 3:
        gasoline_show_data.disp_10data()
    elif choice == 4:
        gasoline_show_chart.chart()
    else:
        break

    x = input("press enter to main menu")