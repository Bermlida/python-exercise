
from Chapter10.modules import selenium_handler

urls = [
    'http://tw.yahoo.com/',
    'https://www.google.com/',
    'https://www.facebook.com/',
    'http://disp.cc/',
    'https://www.ptt.cc/'
]

i = 0
for url in urls:
    print("show webpage: {}" . format(url))
    #selenium_handler.show_webpage(url)
    selenium_handler.show_webpage(url, "resources/webpage{}.png" . format(i))
    i += 1