
from bs4 import BeautifulSoup
from urllib.parse import urlparse
from urllib.request import urlopen
import os
import requests
import sys

if len(sys.argv) < 2:
    print("usage: python 9-6.py <<target url>>")
    exit(1)

url = sys.argv[1]
domain = "{}://{}" . format(urlparse(url).scheme, urlparse(url).hostname)
html = requests.get(url).text
sp = BeautifulSoup(html, 'html.parser')
all_links = sp.find_all(['a', 'img'])

for link in all_links:
    src = link.get('src')
    href = link.get('href')
    targets = [src, href]

    for target in targets:
        if target != None and ('.jpg' in target or '.png' in target):
            if target.startswith('http'):
                full_path = target
            else:
                full_path = domain + target

            image_dir = url.split('/')[-1]
            if not os.path.exists(image_dir):
                os.mkdir(image_dir)

            filename = full_path.split('/')[-1]
            ext = filename.split('.')[-1]
            filename = filename.split('.')[-2]
            if 'jpg' in ext:
                filename = filename + '.jpg'
            else:
                filename = filename + '.png'

            image = urlopen(full_path)
            fp = open(os.path.join(image_dir, filename), 'wb')
            fp.write(image.read())
            fp.close()
