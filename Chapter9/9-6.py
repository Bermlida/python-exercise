
from bs4 import BeautifulSoup
from urllib.parse import urlparse
import requests
import sys

if len(sys.argv) < 2:
    print("usage: python 9-6.py <<target url>>")
    exit(1)

url = sys.argv[1]

domain = "{}://{}" . format(urlparse(url).scheme, urlparse(url).hostname)

html = requests.get(url).text

sp = BeautifulSoup(html, 'html.parser')

all_links = sp.find_all(['a', 'img'])

for link in all_links:
    src = link.get('src')
    href = link.get('href')
    targets = [src, href]

    for target in targets:
        if target != None and ('.jpg' in target or '.png' in target):
            if target.startswith('http'):
                print(target)
            else:
                print(domain + target)

