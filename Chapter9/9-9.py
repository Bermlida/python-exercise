
from bs4 import BeautifulSoup
import requests

pre_html = '''
<!DOCTYPE html>
<html>
    <head>
        <meta charset='utf-8'>
        <title>cpc history oil price</title>
    </head>
    <body>
        <h2>cpc history oil price</h2>
        <table width='600' border='1'>
            <tr><td>date</td><td>92</td><td>95</td><td>98</td></tr>
'''

post_html = '''
        </table>
    </body>
</html>
'''

url = 'http://new.cpc.com.tw/division/mb/oil-more4.aspx'

html = requests.get(url).text
sp = BeautifulSoup(html, 'html.parser')
data = sp.find_all('span', {'id': 'Showtd'})
rows = data[0].find_all('tr')

prices = list()
for row in rows:
    cols = row.find_all('td')
    if len(cols[1].text) > 0:
        item = [cols[0].text, cols[1].text, cols[2].text, cols[3].text]
        prices.append(item)

html_body = ''
for price in prices:
    html_body += "<tr><td>{}</td><td>{}</td><td>{}</td><td>{}</td></tr>" . format(price[0], price[1], price[2], price[3])
html_file = pre_html + html_body + post_html

fp = open('oilprice.html', 'w')
fp.write(html_file)
fp.close()



