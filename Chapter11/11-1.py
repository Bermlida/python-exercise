
from Chapter11.modules import firebase_handler
import time

new_users = [
    {'name': 'Richard Ho'},
    {'name': 'Tom Wu'},
    {'name': 'Judy Chen'},
    {'name': 'Lisa Chang'}
]

for user in new_users:
    firebase_handler.post_data('/users', user)
    time.sleep(3)