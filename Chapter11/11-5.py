
from Chapter11.modules import inv_lotto

while True:
    choice = inv_lotto.disp_menu()

    if choice == 1:
        inv_lotto.enter_lotto()
    elif choice == 2:
        inv_lotto.disp_lotto()
    elif choice == 3:
        inv_lotto.delete_lotto()
    else:
        break

print('manage program close')
