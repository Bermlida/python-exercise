
from Chapter11.modules import firebase_handler

users = firebase_handler.get_data('/users')

for user_key in users:
    print('user({}): {}' . format(user_key, users[user_key]['name']))