
from Chapter11.modules import firebase_handler

choice = input("input inv data(y/Y: yes ; other: not) ")

while choice == 'Y' or choice == 'y':
    inv_lotto = dict()
    inv_lotto['month'] = input("input month: ")
    inv_lotto['p1000w'] = input("input 1000w number:")
    inv_lotto['p200w'] = input("input 200w number:")

    inv_lotto['p20w'] = list()
    p20w_length = int(input("input 20w quantity:"))
    for i in range(1, p20w_length + 1):
        inv_lotto['p20w'].append(input("input 20w number:"))

    inv_lotto['p200'] = list()
    p200_length = int(input("input 200 quantity:"))
    for i in range(1, p200_length + 1):
        inv_lotto['p200'].append(input("input 200 number:"))

    print("those data will save to firebase as follow:")
    print(inv_lotto)

    firebase_handler.post_data('/invlotto/' + inv_lotto['month'], inv_lotto)

    choice = input("continue input inv data(y/Y: yes ; other: not) ")

