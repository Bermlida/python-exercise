
from Chapter11.modules import firebase_handler

def disp_menu():
    print('inv lotto manage')
    print('----------')
    print('1. input number')
    print('2. show number')
    print('3. delete number')
    print('0. exit')
    print('----------')

    choice = input('input your choice: ')
    return int(choice)

def enter_lotto():
    inv_lotto = dict()
    inv_lotto['month'] = input("input month: ")

    old_inv_lotto = firebase_handler.get_data('/invlotto/' + inv_lotto['month'])
    if old_inv_lotto != None:
        print("data exists, press any key to return menu")
        return

    inv_lotto['p1000w'] = input("input 1000w number:")
    inv_lotto['p200w'] = input("input 200w number:")

    inv_lotto['p20w'] = list()
    p20w_length = int(input("input 20w quantity:"))
    for i in range(1, p20w_length + 1):
        inv_lotto['p20w'].append(input("input 20w number:"))

    inv_lotto['p200'] = list()
    p200_length = int(input("input 200 quantity:"))
    for i in range(1, p200_length + 1):
        inv_lotto['p200'].append(input("input 200 number:"))

    print("those data will save to firebase as follow:")
    print(inv_lotto)

    firebase_handler.post_data('/invlotto/' + inv_lotto['month'], inv_lotto)


def disp_lotto():
    lottos = firebase_handler.get_data('/invlotto')

    if lottos == None:
        print('data not exists.')
    else:
        print('data as follow:')

        inv_months = list(lottos.keys())
        for inv_month in inv_months:
            show_lotto(lottos, inv_month)


def delete_lotto():
    lottos = firebase_handler.get_data('/invlotto')

    if lottos == None:
        print('data not exists.')
    else:
        print('those data can delete as follow:')

        inv_months = list(lottos.keys())
        for inv_month in inv_months:
            print('month: ', inv_month)

        target = input("enter month to delete: ")
        if target not in inv_months:
            print("month error, will return menu")
            return

        print('number of month {} as follow:'. format(target))
        show_lotto(lottos, target)

        answer = input('delete those data?(y/n) ')
        if answer == 'y' or answer == 'Y':
            firebase_handler.delete_data('/invlotto/' + target)

def show_lotto(lottos, inv_month):
    key_id = list(lottos[inv_month].keys())[0]
    print('month: ', inv_month)
    print('1000w: {}'.format(lottos[inv_month][key_id]['p1000w']))
    print('200w: {}'.format(lottos[inv_month][key_id]['p200w']))

    print('20w:')
    for i in lottos[inv_month][key_id]['p20w']:
        print(str(i) + "    ", end="")

    print("200:", end="")
    for i in lottos[inv_month][key_id]['p200']:
        print(str(i) + "    ", end="")

    print("\n")



