
from firebase import firebase

db_url = "https://pythonexercise-26204.firebaseio.com/"

firebase_handler = firebase.FirebaseApplication(db_url, None)

def post_data(path, data):
    firebase_handler.post(path, data)

def get_data(path):
    return firebase_handler.get(path, None)

def delete_data(path):
    return  firebase_handler.delete(path, None)