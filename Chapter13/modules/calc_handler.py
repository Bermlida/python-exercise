
import random
import string

def id_generator(size=6, chars=string.ascii_uppercase + string.digits):
    return ''.join(random.choice(chars) for _ in range(size))

def get_school_quantitys(source_file):
    with open(source_file, 'r') as file_open:
        lines = file_open.readlines()

    school_quantitys = list()
    for line in lines:
        school_quantitys.append(int(line.split()[1]))

    return school_quantitys


def get_year_borns(source_file):
    with open(source_file, 'r') as file_open:
        lines = file_open.readlines()

    year_borns = dict()
    for line in lines:
        year, total, boy, girl = line.split()
        year_borns[year] = {'boy': int(boy), 'girl': int(girl)}

    return year_borns


def calc_born_vs_school(born, school):
    return int(float(born) / float(school))

def calc_school_vs_born(school, born):
    return float(float(school) / float(born))