
from Chapter13.modules import calc_handler
import matplotlib.pyplot as pyplot
import numpy

school_quantitys = calc_handler.get_school_quantitys('resources/school.txt')
year_borns = calc_handler.get_year_borns('resources/yrborn.txt')

totals = list()
for year, borns in year_borns.items():
    totals.append(borns['boy'] + borns['girl'])

start_year = int(min(year_borns.keys()))
end_year = int(max(year_borns.keys()))
year_range = numpy.arange(start_year, end_year + 1)

pyplot.subplot(221)
pyplot.plot(year_range, totals)
pyplot.xlim(start_year, end_year)
pyplot.title('2010 - 2015 (Born Total)')
print('Born Total: ', totals)

pyplot.subplot(222)
pyplot.plot(year_range, school_quantitys)
pyplot.xlim(start_year, end_year)
pyplot.title('2010 - 2015 (School Number)')
print('School Number: ', school_quantitys)

list_calc_born_vs_school = list()
for index in numpy.arange(len(totals)):
    list_calc_born_vs_school.append(
        calc_handler.calc_born_vs_school(
            totals[index],
            school_quantitys[index]
        )
    )
pyplot.subplot(223)
pyplot.plot(year_range, list_calc_born_vs_school)
pyplot.xlim(start_year, end_year)
pyplot.title('2010 - 2015 (Born / School)')
print('2010 - 2015 (Born / School)', list_calc_born_vs_school)

list_calc_school_vs_born = list()
for index in numpy.arange(len(totals)):
    list_calc_school_vs_born.append(
        calc_handler.calc_school_vs_born(
            school_quantitys[index],
            totals[index]
        )
    )
pyplot.subplot(224)
pyplot.plot(year_range, list_calc_school_vs_born)
pyplot.xlim(start_year, end_year)
pyplot.title('2010 - 2015 (School / Born)')
print('2010 - 2015 (School / Born):', list_calc_school_vs_born)

pyplot.show()