
from PIL import Image
from PIL import ImageDraw
import glob
import os
import sys

source_dir = 'resources'
logo_file = 'resources/logos/test_logo.png'
if len(sys.argv) > 1:
    source_dir = sys.argv[1]
    if len(sys.argv) == 3:
        logo_file = sys.argv[2]
print('Processing: {}' . format(source_dir))

if not os.path.exists(source_dir):
    print('specified directory not found')
    exit()

target_dir = 'resources/resize_photos'
if not os.path.exists(target_dir):
    os.mkdir(target_dir)

files = glob.glob(source_dir + '/*.jpg') + \
        glob.glob(source_dir + '/*.png')

logo = Image.open(logo_file)
logo = logo.resize((150, 150))
for file in files:
    pathname, filename = os.path.split(file)
    print(filename)

    image = Image.open(file)
    width, height = image.size
    image = image.resize((800, int(800 / float(width) * height)))
    image.paste(logo, (0, 0))
    image.save(target_dir + '/' + filename)
    image.close()
