
import matplotlib.pyplot as pyplot
import numpy

with open('resources/popu.txt', 'r') as file_open:
    populations = file_open.readlines()

city_list = list()
population_list = list()

for population_line in populations:
    city, population = population_line.split(',')
    city_list.append(city)
    population_list.append(int(population))

ind = numpy.arange(len(city_list))

pyplot.bar(ind, population_list)
pyplot.xticks(ind + 0.5, city_list)
pyplot.title('13-2')
pyplot.show()