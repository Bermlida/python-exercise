
from Chapter13.modules import calc_handler
import matplotlib.pyplot as pyplot
import numpy

a = 1.5
b = 1

degree = numpy.linspace(0, 2 * numpy.pi, 200)

x1 = a * (1 + numpy.cos(degree)) * numpy.cos(degree)
y1 = a * (1 + numpy.cos(degree)) * numpy.sin(degree)

x2 = a * numpy.sin(2 * degree)
y2 = b * numpy.sin(degree)

pyplot.xlim(-2, 3.5)
pyplot.ylim(-2.5, 2.5)
pyplot.plot(x1, y1, color='red')
pyplot.plot(x2, y2, color='blue')

choice = input('save picture(y/Y: yes ; other: no): ')
if choice == 'y' or choice == 'Y':
    filename = 'resources/picture' + calc_handler.id_generator() + '.png'
    pyplot.savefig(filename, format="png", dpi=200)
    print('image saved. file path: ', filename)
else:
    pyplot.show()