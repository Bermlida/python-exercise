
import matplotlib.pyplot as pyplot

w = [1, 3, 4, 5, 9, 11]
x = [1, 2, 3, 4, 5, 6]
y = [20, 30, 14, 67, 42, 12]
z = [12, 33, 43, 22, 34, 20]

pyplot.plot(x, y, lw=5, label='Mary')
pyplot.plot(w, z, lw=1, label='Tom')

pyplot.xlabel('month')
pyplot.ylabel('dollars (million)')

pyplot.legend()
pyplot.title('13-1')

pyplot.show()
