
from PIL import Image
from PIL import ImageDraw

image = Image.open('resources/picture7T5FHZ.png')
width, height = image.size
image_draw = ImageDraw.Draw(image)

image_draw.line((0, 0, width, height), width = 20, fill = (255, 0, 0))
image_draw.line((width, 0, 0, height), width = 20, fill = (255, 0, 0))
image_draw.ellipse((50, 50, width - 50, height - 50), outline = (255, 255, 0))
image_draw.text((100, 100), 'This is a test image')

image.show()