
import matplotlib.pyplot as pyplot
import numpy

degree = numpy.linspace(0, 2 * numpy.pi, 200)
x = numpy.cos(degree)
y = numpy.sin(degree)

pyplot.xlim(-1.5, 1.5)
pyplot.ylim(-1.5, 1.5)

pyplot.plot(x, y, 'bo')
pyplot.plot(0.5 * x, 1.5 * y, 'ro')

pyplot.show()