
from PIL import Image
import matplotlib.pyplot as pyplot
import numpy

sample = Image.open('resources/pictureHI9725.png')
sample_convert = sample.convert('L')
width, height = sample_convert.size

crop = sample_convert.crop((width / 2 - 300, height / 2 - 300, width / 2 + 300, height / 2 + 300))
crop_histogram = crop.histogram()

original = sample.resize((600, 600))
original_convert = original.convert('L')
original_histogram = original_convert.histogram()

r, g, b, test = original.split()
r_histogram = r.histogram()
g_histogram = g.histogram()
b_histogram = b.histogram()

graph_range = numpy.arange(0, len(crop_histogram))
pyplot.plot(graph_range, crop_histogram, color='cyan', label='cropped')
pyplot.plot(graph_range, original_histogram, color='black', label='original')
pyplot.plot(graph_range, r_histogram, color='red', label='Red Plane')
pyplot.plot(graph_range, g_histogram, color='green', label='Green Plane')
pyplot.plot(graph_range, b_histogram, color='blue', label='Blue Plane')

pyplot.xlim(0, 255)
pyplot.ylim(0, 8000)
pyplot.legend()
pyplot.show()
