
import matplotlib.pyplot as pyplot
import numpy

with open('resources/yrborn.txt', 'r') as file_open:
    lines = file_open.readlines()

year_borns = dict()
for line in lines:
    year, total, boy, girl = line.split()
    year_borns[year] = {'boy': int(boy), 'girl': int(girl)}

totals = list()
boys = list()
girls = list()
for year, borns in year_borns.items():
    totals.append(borns['boy'] + borns['girl'])
    boys.append(borns['boy'])
    girls.append(borns['girl'])

start_year = int(min(year_borns.keys()))
end_year = int(max(year_borns.keys()))
year_range = numpy.arange(start_year, end_year + 1)

pyplot.subplot(211)
pyplot.plot(year_range, totals)
pyplot.xlim(start_year, end_year)
pyplot.title('2010 - 2015 (Total)')

offset = 0.05
width = 0.1
pyplot.subplot(212)
pyplot.bar(year_range - offset, boys, width, color='b')
pyplot.bar(year_range + offset, girls, width, color='r')
pyplot.xlim(start_year - 1 , end_year + 1)
pyplot.title('2010 - 2015 (Boy:Girl)')

pyplot.show()