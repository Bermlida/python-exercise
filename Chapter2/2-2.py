import datetime

today = datetime.date.today()
month = int(input("enter month: "))
day = int(input("enter day: "))
birthday = datetime.date(today.year, month, day)

if birthday < today:
    birthday = datetime.date(today.year + 1, month, day)

diff = birthday - today
if(diff.days == 0):
    print("today is your birthday")
else:
    print("after " + str(diff.days) + " day is your birthday")