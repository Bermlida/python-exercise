
from Chapter12.modules import exif_handler
import glob
import os
import shutil
import sys

if len(sys.argv) < 2:
    print('Usage: python 12-6.py <source_dir>')
    exit()

source_dir = sys.argv[1]
#source_dir = "C:\\Users\\suser\\OneDrive"
files = glob.glob(source_dir + '/*.jpg') + \
        glob.glob(source_dir + '/*.png') + \
        glob.glob(source_dir + '/*.bmp') + \
        glob.glob(source_dir + '/*.tif')

if not os.path.exists('resources/photos'):
    os.mkdir('resources/photos')

for file in files:
    filename = file.split('\\')[-1]
    year, month = exif_handler.get_year_month(file)

    target_dir = 'resources/photos/' + year + '/' + month
    if not os.path.exists(target_dir):
        os.makedirs(target_dir, exist_ok=True)

    i = 0
    original_filename = filename
    while True:
        if not os.path.exists(target_dir + '/' + filename):
            shutil.copy(file, target_dir + '/' + filename)
            print(filename)
            break
        else:
            extname = '.' + original_filename.split('.')[-1]
            filename = original_filename.split(extname)[0] + str(i) + extname
            i = i + 1