
from Chapter12.modules import facebook_handler

attachment = {
    'name': '大佛頂首楞嚴經講義',
    'link': 'http://book.bfnn.org/article2/1472.htm',
    'caption': '般若文海',
    'description': '圓瑛法師者，今佛門之龍象也。慧性天生，辯才無礙，宏施法雨，中外咸沾，著作行世，十有餘種。十二年前，曾撰楞嚴綱要一書，明燈普照，廣被遐邇。迨年六十八，始行註釋此經，以四十餘年之鑽研，究厥精微，編成講義，大願既償，囑為之序。竊念學佛信眾，苟無南針，而欲深入經藏，譬靡管而窺大，棄蠡以測海，求能瞭解妙理，誠恐北轍南轅！‧‧‧',
    'picture': 'http://book.bfnn.org/article2/bookimages/1472.jpg'
}

facebook_handler.put_to_wall(
    '大佛頂首楞嚴經講義 ( 晨鐘暮鼓中……共－－－－－',
    attachment
)

print('貼文已發佈，請至 facebook 查閱')