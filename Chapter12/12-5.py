
from Chapter12.modules import facebook_handler
import requests
import shutil

photos = facebook_handler.get_photos()
photos = photos['data']

for photo in photos:
    image = photo['images'][0]

    filename = image['source'].split('/')[-1].split('?')[0]
    print(filename)

    file = open('resources/fb-image/' + filename, 'wb')
    picture = requests.get(image['source'], stream=True)
    shutil.copyfileobj(picture.raw, file)
    file.close()
