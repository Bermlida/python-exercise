
import glob
import hashlib
import os

files = glob.glob('resources/photos/*.jpg') + \
        glob.glob('resources/photos/*.png')

md5s = dict()

for file in files:
    print('file is processing......')
    md5 = hashlib.md5(open(file, 'rb').read()).digest()

    if md5 in md5s:
        print('File duplication: {}, {}' . format(os.path.abspath(file), md5s[md5]))
    else:
        md5s[md5] = os.path.abspath(file)
