
from bs4 import BeautifulSoup
import jieba
import operator
import requests

url = 'http://www.appledaily.com.tw/realtimenews/section/new/'

news_page = requests.get(url)

news = BeautifulSoup(news_page.text, 'html.parser')

titles = news.find_all('a', {'target': '_blank'})

headlines = ''
for title in titles:
    title = title.find_all('font', {'color': '#383c40'})

    if len(title) > 0:
        headlines += title[0].text

words = jieba.cut(headlines)

word_count = dict()
for word in words:
    if word in word_count.keys():
        word_count[word] += 1
    else:
        word_count[word] = 1

    sorted_wc = sorted(word_count.items(), key=operator.itemgetter(1), reverse=True)

for item in sorted_wc:
    if item[1] > 1:
        print(item)
    else:
        break