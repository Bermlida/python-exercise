
import exifread
import os
import time

def get_year_month(fullpath):
    file = open(fullpath, 'rb')
    exif = exifread.process_file(file)
    year_month = 0

    if 'EXIF DateTimeOriginal' in exif:
        year_month = exif['EXIF DateTimeOriginal'].values
    else:
        year_month = time.strftime('%Y:%m:%d', time.localtime(os.stat(fullpath).st_ctime))

    file.close()

    return year_month[0:4], year_month[5:7]