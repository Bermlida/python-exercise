
import facebook

token = 'EAACEdEose0cBAOySHiMZBeFIQMh3Pq5juRPI3SJeA1RBZCTHxZA7AMakQL8KpjZBQpaBgl61Yps7HMgJpW4xXJXi93ioZAB6heH9CSbMaZBpdobLByoPHCBgPFi37s9UDOJ4zGpaniYVYavfF2t8BQuvTe6QG3JcVmDnC7QLYcwzT8WASiSB8vGy0nyN7j22gZD'

def get_photos():
    graph = facebook.GraphAPI(access_token=token)

    return graph.get_connections(id='me', connection_name='photos')

def get_posts():
    graph = facebook.GraphAPI(access_token=token)

    return graph.get_connections(id='me', connection_name='posts')


def put_to_wall(message, attachment = None):
    graph = facebook.GraphAPI(access_token=token)

    if attachment != None:
        graph.put_wall_post(message=message, attachment=attachment)
    else:
        graph.put_wall_post(message=message)

def put_like(post_id):
    graph = facebook.GraphAPI(access_token=token)

    graph.put_like(post_id)