
import ast
import sys


if len(sys.argv) < 2:
    print("usage: python 8-5.py grade file")
    exit(1)

scores = dict()
with open(sys.argv[1], 'r') as fp:
    filedata = fp.read()
    scores = ast.literal_eval(filedata)

print("{} grade file as follow:" . format(sys.argv[1]))
print(scores)