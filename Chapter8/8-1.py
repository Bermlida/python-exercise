
#fp = open("resources/test.txt", "r")
#test = fp.readlines()
#fp.close()

i = 1
print("test.txt content:")

#for text in test:
#    print("test.txt line {}: {}" . format(i, text), end = "")
#    i += 1

with open('resources/test.txt') as fp:
    for text in fp.readlines():
        print("test.txt line {}: {}".format(i, text), end="")
        i += 1