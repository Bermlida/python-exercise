
import datetime
import json

fp = open('resources/earthquake.json', 'r')

earthquakes = json.load(fp)

print('earthquake record as follow:')

for eq in earthquakes['features']:
    print("Place:{}" . format(eq['properties']['place']))
    print("Mag:{}".format(eq['properties']['mag']))

    et = float(eq['properties']['time']) / 1000.0
    d = datetime.datetime.fromtimestamp(et) . strftime('%Y-%m-%d %H:%M:%S')
    print("DateTime:{}" . format(d))