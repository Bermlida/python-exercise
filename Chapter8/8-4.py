
import sys

if len(sys.argv) < 2:
    print("usage: python 8-4.py grade file")
    exit(1)

no = 1
scores = dict()
while True:
    score = int(input("input the grade of no {}:(-1 to end)" . format(no)))
    if score == -1:
        break
    scores[no] = score
    no += 1

with open(sys.argv[1], 'w') as fp:
    fp.write(str(scores))

print("{} already saved" . format(sys.argv[1]))

